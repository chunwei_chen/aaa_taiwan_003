// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SlashTap_421GameMode.generated.h"

UCLASS(minimalapi)
class ASlashTap_421GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASlashTap_421GameMode();
};



