// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "SlashTap_421GameMode.h"
#include "SlashTap_421Character.h"
#include "UObject/ConstructorHelpers.h"

ASlashTap_421GameMode::ASlashTap_421GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
